# Log Microservice 

Logs Microservice responsible for storig and retriveing logs and send E-mails to Administrators. Logs Microservices work baed on job queues to handle concurrent requests . 
## Installation
You can run Log Microservice in this following simple steps :

```sh
$ git clone  https://gitlab.com/mahmoudsror/logmicroservice.git

$ cd logmicroservice/

$ cp backend/Dockerfile.dev backend/Dockerfile
$ cp .env.dev .env

$ docker-compose up -d --build

```

Now you should have Logs Microsrvice (backend - Database - Redis) containers up 

## Usage

It is time to store logs to LogsMicroservice through its API, 

Use this Url For documentation to : http://localhost:4000/api-docs/#/


## Testing 

To run tests follow below steps :
```sh

$ cd logmicroservice/

$ cp backend/Dockerfile.test backend/Dockerfile

$ cp .env.test .env

$ docker-compose up -d --build

$ docker-compose exec backend sh -c "npm run test"

```
