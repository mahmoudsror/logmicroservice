const chai =  require('chai');
const supertest = require('supertest');
const app = require('../../app');
const api = supertest(app);
chai.should();
const commonHeaders= { 
  Authorization: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NTk3MDIwNjgyOTcsImV4cCI6MTU1OTc4ODQ2ODI5N30.fqk5Qcen4wDmq_LTTv_mVEA2026BM5nPBUTZRByNNNE"
}

describe('Logs', () => {
  
  it("should return Logs from DB ", (done) => {
    api.get('/logs')
    .set(commonHeaders)
      .end((err, res) => {
        res.status.should.be.equal(200);
        res.body.should.be.a('object');
        res.body.should.have.property('data')
        done();
      });
    });
    it("should seearch for logs acording to query params ", (done) => {
      api.get('/logs/search')
      .query({title: "test"})
      .set(commonHeaders)
        .end((err, res) => {
          res.status.should.be.equal(200);
          res.body.should.be.a('object');
          res.body.should.have.property('data')
          done();
        });
      });

    it("should creates Logs  ", (done) => {
      api.post('/logs')
      .set(commonHeaders)
      .send({
        title: "test",
        description: "test description",
        path:" /path/to/logs",
        status_code:"500"
      })
        .end((err, res) => {
          res.status.should.be.equal(200);
          res.body.should.be.a('object');
          res.body.should.have.property('data')
          done();
        });
      });
  });