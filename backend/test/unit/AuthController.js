const chai =  require('chai');
const supertest = require('supertest');
const app = require('../../app');
const api = supertest(app);
chai.should();

describe('Auth', () => {
  
  it("should return JWT token ", (done) => {
    api.get('/getToken')
      .end((err, res) => {
        res.status.should.be.equal(200);
        res.body.should.be.a('object');
        res.body.should.have.property('token')
        done();
      });
    });
  });