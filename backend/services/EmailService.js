const nodemailer = require('nodemailer');

class EmailService {
  /**
   * Adds job to queue
   * 
   * @param {Object} First  queue object
   * @param {Object} second  Logs object
   * 
   */
  addMailJob(queue, logsInfo) {
    queue.create(`email`,
    {
      ...logsInfo
    })
    .removeOnComplete(true)
    .save();
    this.processMail(queue)
  }

  /**
   * get node mailer transport object
   * 
   * @returns {Object} Nodemailer transport Object
   * 
   */
  getTransporter() {
    const transporter = nodemailer.createTransport({
     host: process.env.EMAIL_HOST,
     port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASS
      }
     });
    return transporter;
  }

  /**
   * Pocess jobs in mail queue
   * 
   * @param {Object} First  queue object
   * @returns {function} returns done() cb in success or fail
   * 
   */
  processMail(queue) {
    queue.process('email',10, async (job, done)=>{
      if(job.type == 'email'){

      const isSent = await this.sendEmail(job.data);
      console.log("is sent ===> ", isSent)

      if(isSent){
        return done()
      }
      return done(new Error('Error sending e-mail '))
      }
    });
  }

  /**
   * Pocess jobs in mail queue
   * 
   * @param {Object} First  Logs Data to send to admin
   * @returns {Boolean} returns true if mail sent or false if mail failed
   * 
   */
  sendEmail(emailData) {
    const receiversList = [
      'mahmoudsror@gmail.com'
    ]
    const mailOptions ={
      from: "Logs service ",
      subject: emailData.title,
      body: emailData.description,
      to:receiversList
    }
    const transporter = this.getTransporter();
    return transporter
    .sendMail(mailOptions)
    .then(res=>{
      return true
    }).catch(err=>{
      return false
    });
  }
}
module.exports = EmailService;