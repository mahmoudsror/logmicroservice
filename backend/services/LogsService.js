const path = require('path');
const Logs = require(path.resolve('models','Logs.js'));
const Mailer = require(path.resolve('services','EmailService.js'));
const EmailService = new Mailer();
class LogsService {


  /**
   * Get all Logs from DB
   * 
   * @returns {Object} returns all logs from DB
   * 
   */
  async list() {

    const logsList = await Logs.find({});
    return logsList;
  }

  /**
   * Get  Logs from DB based on requesrt query
   * 
   * @returns {Object} returns matched logs from DB
   * 
   */  
  async getLogs(params){
    const logsList = await Logs.find(params);
    return logsList;
  }

  /**
   * Pocess jobs in create queue
   * 
   * @param {Object} First  queue object
   * @returns {function} returns done() cb in success or fail
   * 
   */  
  processLogs (queue) {

    queue.process('create', 10, async (job,done)=>{
    
      const savedLogs = await this.saveLogsToDB(job.data.data, done);
      
      if(!savedLogs.status){
        return done(savedLogs.err);  
      }

      EmailService.addMailJob(queue, savedLogs.data._doc);
      return done(null,savedLogs.data);
    
    });

  }
  /**
   * Save logs data to DB
   * 
   * @param {Object} First  
   * @returns {Object} returns result object or error object
   * 
   */
  saveLogsToDB(logsData) {

    return Logs.create(logsData).then(res=> {
      if(res){
        return {
          status: true,
          data:res
        }
      }
      return {
        status: false,
        data:res
      }
    
    }).catch(err=>{
      return {
        status: false,
        err: err
      };
    });

  } 
}

module.exports = LogsService;
