const path = require('path');
  kue = require('kue'),
  Logs = require(path.resolve('services','LogsService.js')),
  LogsService = new Logs();

let queue = kue.createQueue({
  prefix: 'q',
  redis: {
    port: 6379,
    host: 'redis'
  }
});

class LogsController {

  /**
   * Get all Logs from logs service
   * 
   * @param {req} First  request
   * @param {res} second  response 
   * @returns {Object} returns json object contains all logs
   * 
   */  
  async list (req,res) {

    const allLogs = await LogsService.list();
    
    return res
      .status(200)
      .send({
        status:200,
        data:allLogs
      });
    
  }

  /**
   * Get all Logs from logs service based on request query params
   * 
   * @param {req} First  request
   * @param {res} second  response 
   * @returns {Object} returns json object contains all logs matches search query
   * 
   */  
  async search (req,res) {

    const searchResults = await LogsService.getLogs(req.query);
    
    return res
      .status(200)
      .send({
        status:200,
        data:searchResults
      });
    
  }


  /**
   * Pass logs to job queue to save it to DB
   * 
   * @param {req} First  request
   * @param {res} second  response 
   * @returns {Object} returns json object contains created data
   * 
   */    
  async store(req,res) {

    let job = queue.create(`create`,
    {
      data:req.body
    })
    .removeOnComplete(true)
    .save();
    
    await LogsService.processLogs(queue);
    
    job.on('complete', (result)=>{
      
      res
      .status(200)
      .send({
        status:200,
        data: result
      });
     
    }).on('failed', (errorMessage)=>{
      
      res
      .status(500)
      .send({
        status:500,
        data: errorMessage
      });
     
    });
    
  }
}
module.exports = LogsController