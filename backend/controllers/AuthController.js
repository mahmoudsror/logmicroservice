const jwt = require('jsonwebtoken');

class AuthController {

  /**
   * Generate JWT token to user
   * 
   * @param {req} First  request
   * @param {res} second  response 
   * @returns {Object} returns json object contains jwt token
   * 
   */
  generateToken(req,res) {
    const token= jwt.sign({
      iat: new Date().getTime(),
      exp: new Date().setDate(new Date().getDate() + 1)
    },
    process.env.AUTH_SECRET);
    return res.send({token: token});
  }
}

module.exports = AuthController