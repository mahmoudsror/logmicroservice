const mongoose = require('mongoose');
const LogsSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  status_code: {
    type: String,
    required: true
  },
  path:{
    type:String,
    required: true
  }
},
{ timestamps: true });

LogsSchema.index({status_code: 1, title: 1, created_a:1 });
const Logs = mongoose.model('Logs',LogsSchema)

module.exports = Logs
