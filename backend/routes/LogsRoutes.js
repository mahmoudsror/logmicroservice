const path = require('path'),
  auth = require(path.resolve('middleware','authorization.js')),
  validation = require(path.resolve('middleware','validate.js')),
  LogsController = require(path.resolve('controllers','logsController.js'));
  const logsController = new LogsController();
module.exports = function(app){

  app.get('/logs' ,auth.verify, logsController.list);
  app.get('/logs/search', auth.verify, validation.search,logsController.search)
  app.post('/logs' ,auth.verify, logsController.store);

}
