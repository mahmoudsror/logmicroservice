const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../docs/swagger.json');
module.exports = function(app) {
  
  require('./LogsRoutes.js')(app);
  require('./AuthRoutes')(app);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  app.get('/',function(req,res) {
    res.send("Welcome to Log Microservice");
  });
  app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
}
