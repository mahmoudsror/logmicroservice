const { check, oneOf, validationResult } = require('express-validator/check');
module.exports = {
  createLogs: function(req,res,next){
    req.checkBody('title')
      .notEmpty()
      .withMessage('blank')
      .exists()
      .withMessage('title is required');

    req.checkBody('description')
      .notEmpty()
      .withMessage('blank')
      .exists()
      .withMessage('description is required');

    req.checkBody('status_code')
      .notEmpty()
      .withMessage('blank')
      .exists()
      .withMessage('status_code is required');

    req.checkBody('path')
      .notEmpty()
      .withMessage('blank')
      .exists()
      .withMessage('path is required'); 
    
    let errors = req.validationErrors();
    
    if(errors){
      return res.status(400).send({
        status:400,
        errors: errors
      });
    }
    next()
  },

  search: function(req,res,next){
    if( req.checkQuery('title').exists() || req.checkQuery('status_code').exists() || req.checkQuery('created_at').exists()){
      next();
    }else{
      return res.status(400).send({
        status:400,
        errors: " You must pass one of (title, status_code , created_at)  to search query params"
      });
    }
     

  }
}
