const jwt = require('jsonwebtoken');
module.exports = {
  verify:(req,res,next)=>{

    if( !req.headers['authorization'] ){
      return res.status(401).send('Authorization token not sent in headers ')
    }
    const isValid = jwt.verify(req.headers['authorization'], process.env.AUTH_SECRET);
    if( !isValid ){
      return res.status(401).send('Unauthorized ');
    }
    
    next();
  }
}
